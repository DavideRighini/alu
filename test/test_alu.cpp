#include <systemc.h>
#include "alu.hpp"

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

static const unsigned TEST_SIZE = 6;
static const unsigned DATA_WIDTH = 16;
static const unsigned DATA_ALU = 6;
sc_fifo<int> observer_alu;
int values1[TEST_SIZE]; //vettore usato per tutti i test
int values2[TEST_SIZE]; //vettore usato per tutti i test

SC_MODULE(TestBench) 
{
 public:
 
	//canali per l'ALU
	sc_signal<sc_lv<DATA_WIDTH> > alu_op1;
	sc_signal<sc_lv<DATA_WIDTH> > alu_op2;
	sc_signal<sc_lv<DATA_ALU> > alu_func;
	sc_signal<sc_lv<DATA_WIDTH> > alu_result;
	sc_signal<bool> alu_zero;	

	Alu alu1; //ALU

  SC_CTOR(TestBench) : alu1("alu1")
  {
 		init_values();
 		
 		//Alu:
			SC_THREAD(init_values_alu_test);
			alu1.op1(this->alu_op1);
			alu1.op2(this->alu_op2);
			alu1.func(this->alu_func);
			alu1.result(this->alu_result);
			alu1.zero(this->alu_zero);		
			SC_THREAD(observer_thread_alu);	
				sensitive << alu1.result << alu1.zero;

  }

	void init_values() {
				values1[0] = 5;
				values1[1] = 4;
				values1[2] = 3;
				values1[3] = 2;
				values1[4] = 1;
				values1[5] = 0;
				
				values2[0] = 1;
				values2[1] = 3;
				values2[2] = 5;
				values2[3] = 7;
				values2[4] = 9;
				values2[5] = 0;
						  		  
	}

	void init_values_alu_test() {
		for (unsigned i=0;i<TEST_SIZE;i++) {
		  alu_op1.write(values1[i]); 
		  alu_op2.write(values2[i]); 
		  alu_func.write(10); //sub  
		  wait(10,SC_NS);
		}
		wait(50,SC_NS);
		sc_stop();
	}

	void observer_thread_alu() {
		while(true) {
				wait();
				int value = alu1.result->read().to_int();
				cout << "\nobserver_thread: at " << sc_time_stamp() << " alu out: " << value << endl;
				cout << "observer_thread: at " << sc_time_stamp() << " alu in1: " << alu1.op1->read() << " alu in2: " << alu1.op2->read() << endl;
				cout << "observer_thread: at " << sc_time_stamp() << " alu zero: " << alu1.zero << endl;
				cout << "observer_thread: at " << sc_time_stamp() << " alu func: " << alu1.func->read() << "\n\n";
				if (observer_alu.num_free()>0 ) observer_alu.write(value);
		} 
	}

	int check_alu() {
		int test=0;
		for (unsigned i=0;i<TEST_SIZE;i++) {
				test=observer_alu.read(); //porta fifo
		    if (test != (values1[i] - values2[i]) )
		        return 1;
		}
		return 0;
	}

};
// ---------------------------------------------------------------------------------------

int sc_main(int argc, char* argv[]) {


	TestBench testbench("testbench");
	sc_start();

  return testbench.check_alu();
}
