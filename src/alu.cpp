#include <systemc.h>
#include "alu.hpp"

const unsigned RITARDO = 6;

void Alu::update_ports() {
	sc_lv<DATA_WIDTH> tmpt;
	int tmp;
	int Iop1 = op1->read().to_int();
	int Iop2 = op2->read().to_int();
	unsigned Ifunc = func->read().to_uint();
	unsigned error=0;
	
	while(true) {
		wait();
		wait(RITARDO,SC_NS); //tempo di ritardo
		Iop1 = op1->read().to_int();
		Iop2 = op2->read().to_int();
		Ifunc = func->read().to_uint();
		
		switch (Ifunc) {
			case 5 : //add
				tmp = Iop1 + Iop2;
				break;
			case 10 : //subtract
				tmp = Iop1 - Iop2;
				break;		
			case 15 : //nand
				tmp = ~(Iop1 & Iop2);
				break;
			case 20 : //pass
				tmp = Iop1 ;
				break;
			default :
				cout << "error unknown alu function\n";
				error = 1;
		}

		if (error != 1) {
			result->write(tmp);

			if (tmp == 0) zero->write(1);
			else zero->write(0);
		}
		error=0; 
	
		wait(SC_ZERO_TIME);		
	}
}
