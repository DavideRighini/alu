#ifndef ALU_HPP
#define ALU_HPP

SC_MODULE(Alu) {
	static const unsigned DATA_WIDTH = 16;
	static const unsigned FUNC_WIDTH = 6;
		
	sc_in<sc_lv<DATA_WIDTH> > op1;
	sc_in<sc_lv<DATA_WIDTH> > op2;
	sc_in<sc_lv<FUNC_WIDTH> > func;
	sc_out<sc_lv<DATA_WIDTH> > result;
	sc_out<bool> zero;
	
	
	SC_CTOR(Alu) {
		SC_THREAD(update_ports);
			sensitive << op1 << op2 << func;
	}
	
	private:
	void update_ports();
};


#endif
